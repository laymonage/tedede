function delayedShowPage() {
  var time = setTimeout(showPage, 2000);
}

function showPage() {
  document.getElementsByClassName("loader")[0].style.display = "none";
  wrapper = document.getElementById("wrapper");
  if (wrapper.style.removeProperty) {
    wrapper.style.removeProperty('display');
  } else {
    wrapper.style.removeAttribute('display');
  }
}
