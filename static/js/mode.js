function mode(mode) {
    if (mode == 'dark') {
        $('#mode-text').html('gelap');
        $('body').addClass('dark-mode');
        $('table').addClass('table-dark');
        $('thead').removeClass('thead-light').addClass('thead-dark');
        $('.progress').addClass('dark-mode');
        $('.card').addClass('bg-semidark');
        $('.accordion-btn').addClass('text-white');
        $('.text-secondary').addClass('text-semilight');
        $('.text-semilight').removeClass('text-secondary');
        $('.btn-outline-dark').addClass('btn-outline-light');
        $('.btn-outline-light').removeClass('btn-outline-dark');
        $('#gh-path').addClass('fill-light');
        $('#gh-path').removeClass('fill-dark');
    } else if (mode == 'light') {
        $('#mode-text').html('terang');
        $('body').removeClass('dark-mode');
        $('.text-semilight').addClass('text-secondary');
        $('.text-secondary').removeClass('text-semilight');
        $('table').removeClass('table-dark');
        $('thead').addClass('thead-light').removeClass('thead-dark');
        $('.progress').removeClass('dark-mode');
        $('.card').removeClass('bg-semidark');
        $('.accordion-btn').removeClass('text-white');
        $('.btn-outline-light').addClass('btn-outline-dark');
        $('.btn-outline-dark').removeClass('btn-outline-light');
        $('#gh-path').addClass('fill-dark');
        $('#gh-path').removeClass('fill-light');
    }
}

mode(localStorage.getItem('colorMode'));
if (localStorage.getItem('colorMode') == 'dark') {
    $('#mode-checkbox').attr('checked', true);
    $('#mode-checkbox').parent().addClass('active');
}

function switchMode(object) {

    if (object.prop('checked')) {
        localStorage.setItem('colorMode', 'dark');
        mode('dark');
    }
    else {
        localStorage.setItem('colorMode', 'light');
        mode('light');
    }

}

$('#mode-checkbox').change(function() {
    switchMode($(this));
});
