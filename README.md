# tedede

[![pipeline status](https://gitlab.com/laymonage/tedede/badges/master/pipeline.svg)](https://gitlab.com/laymonage/tedede/commits/master)
[![coverage report](https://gitlab.com/laymonage/tedede/badges/master/coverage.svg)](https://gitlab.com/laymonage/tedede/commits/master)

Belajar Test-Driven Development (TDD) dengan Django.

## [Live app on heroku](https://tedede.herokuapp.com)
