from django.contrib.auth.views import LogoutView
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import redirect, render, reverse


def login(request):
    if request.user.is_authenticated:
        return redirect(reverse('books:index'))
    return render(request, 'oauth/login.html')


@login_required
def welcome(request):
    response = render(request, 'oauth/welcome.html')
    response['Refresh'] = f"3;URL={reverse('books:index')}"
    return response


@login_required
def logout(request):
    response = LogoutView.as_view(template_name='oauth/logout.html')(request)
    response['Refresh'] = f"3;URL={reverse('books:index')}"
    return response


def is_logged_in(request):
    if request.user.is_authenticated:
        return JsonResponse({'loggedIn': True})
    return JsonResponse({'loggedIn': False})
