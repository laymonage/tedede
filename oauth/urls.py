from django.urls import path
from . import views

app_name = 'oauth'
urlpatterns = [
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('welcome/', views.welcome, name='welcome'),
    path('is_logged_in/', views.is_logged_in, name='is_logged_in')
]
