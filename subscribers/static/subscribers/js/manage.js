function appendItemToTable(tbody, i, item) {
    var th_no = $('<th>').attr({'scope': 'row'}).text(i + 1);
    var td_name = $('<td>').text(item.name);
    var td_email = $('<td>').text(item.email);
    var td_unsub = $('<td>').append(
        $('<button>').attr(
            {
                'class': 'btn btn-danger font-weight-medium',
                'onclick': `unsubscribe("${item.email}")`,
                'data-toggle': 'modal',
                'data-target': '#unsub-modal'
            }
        ).text('Hentikan')
    );
    var $tr = $('<tr>').append(
        th_no,
        td_name,
        td_email,
        td_unsub
    ).appendTo(tbody);
}


function writeResultsTable(items) {
    var tbody = $('#tbody-results');
    tbody.empty();
    $(function() {
        $.each(items, function(i, item) {
            appendItemToTable(tbody, i, item);
        });
    });
}


function unsubscribe(email) {
    $('#id_email').attr({'value': email});
    $('#subscriber_email').text(email);
}


function togglePassword() {
    if ($('#id_password').attr('type') === 'password') {
        $('#id_password').attr('type', 'text');
    } else {
        $('#id_password').attr('type', 'password');
    }
}


function repopulateTable() {
    $.ajax({
        type        : 'GET',
        url         : 'list',
        dataType    : 'json',
    })
    .done(function(data) {
        writeResultsTable(data.subscribers);
    });
}


$(document).ready(function() {

    repopulateTable();

    $('form').submit(function(event) {
        var formData = $(this).serializeArray();
        $('#fail-message').hide();
        $.ajax({
            type: 'POST',
            url: 'unsubscribe/',
            data: formData,
            dataType: 'json',
        }).done(function(response) {
            repopulateTable();
            $('form')[0].reset();
            $('#unsub-modal').modal('hide');
        }).fail(function(response) {
            $('#fail-message').show();
        });
        event.preventDefault();
    });

});
