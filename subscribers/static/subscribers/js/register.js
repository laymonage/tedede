nameIsValid = false;
emailIsValid = false;
passwordIsValid = false;

$('form').on('input change paste', '#id_name', function(){
    validateName($('#id_name').val());
});

$('form').on('focusout', '#id_email', function(){
    validateEmail($('#id_email').val());
});

$('form').on('input change paste', '#id_password', function(){
    validatePassword($('#id_password').val());
});

$('form').on('input change paste', '#id_name, #id_email, #id_password', function() {
    toggleSubmit();
});


$(document).ready(function() {
    if ($('#id_name').val().length) {
        validateName($('#id_name').val());
    }
    if ($('#id_email').val().length) {
        validateEmail($('#id_email').val());
    }
    if ($('#id_password').val().length) {
        validatePassword($('#id_password').val());
    }
    toggleSubmit();

    $('form').submit(function(event) {
        var formData = $(this).serializeArray();
        $('#success-message').hide();
        $('#fail-message').hide();
        $.ajax({
            type: 'POST',
            url: 'submit',
            data: formData,
            dataType: 'json',
        }).done(function(response) {
            $('#success-message').show();
            $('form')[0].reset();
        }).fail(function(response) {
            $('#fail-message').show();
            validateAll();
            toggleSubmit();
        });
        event.preventDefault();
    });
});


function togglePassword() {
    if ($('#id_password').attr('type') === 'password') {
        $('#id_password').attr('type', 'text');
    } else {
        $('#id_password').attr('type', 'password');
    }
}


function toggleSubmit() {
    if (nameIsValid && emailIsValid && passwordIsValid) {
        $('#id_submit').prop('disabled', false);
        $('#fail-message').hide();
    } else {
        $('#id_submit').prop('disabled', true);
        $('#success-message').hide();
    }
}


function validateAll() {
    validateName($('#id_name').val());
    validateEmail($('#id_email').val());
    validatePassword($('#id_password').val());
}


function validateName(name) {
    emptyOrCreateErrorDiv('name');
    if (name.length > 0 && name.length <= 64) {
        nameIsValid = true;
    } else {
            createAppendErrorMessage(
                name.length <= 0 ? "Bidang ini tidak boleh kosong." :
                "Pastikan nilai ini mengandung paling banyak 64 karakter " +
                "(sekarang " + name.length + " karakter).", $('#name_errors')
            );
        nameIsValid = false;
    }
}


function validateEmail(email) {
    return $.ajax({
        type        : 'GET',
        url         : 'check_email?email=' + email,
        dataType    : 'json',
    }).then(function(response) {
        if (response.valid && !response.exists) {
            $('#email_errors').empty();
            emailIsValid = true;
            toggleSubmit();
        } else {
            emptyOrCreateErrorDiv('email');
            $.each(response.messages, function(i, message) {
                createAppendErrorMessage(message, $('#email_errors'));
            });
            emailIsValid = false;
            toggleSubmit();
        }
    });
}


function validatePassword(password) {
    emptyOrCreateErrorDiv('password');
    if (password.length >= 8 && password.length <= 32) {
        $('#password_errors').empty();
        passwordIsValid = true;
    } else {
        createAppendErrorMessage(
            password.length <= 0 ? "Bidang ini tidak boleh kosong." :
            password.length < 8 ?
            "Panjang kata sandi harus 8 atau lebih (sekarang "
            + password.length + ")." :
            "Pastikan nilai ini mengandung paling banyak 32 karakter " +
            "(sekarang " + password.length + " karakter)."
            , $('#password_errors')
        );
        passwordIsValid = false;
    }
}


function emptyOrCreateErrorDiv(field) {
    if ($('#' + field + '_errors').length) {
        $('#' + field + '_errors').empty();
    } else {
        $('#form_' + field).after($('<div>').attr({'id': field + '_errors'}));
    }
}


function createErrorMessage(message) {
    return $('<p>')
    .attr({'class': 'font-weight-medium text-danger'})
    .text(message);
}


function createAppendErrorMessage(message, appendTo) {
    createErrorMessage(message).appendTo(appendTo);
}
