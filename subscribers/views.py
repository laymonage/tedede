from django.core.exceptions import ValidationError
from django.core.serializers import serialize
from django.core.validators import MaxLengthValidator, validate_email
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect

from subscribers.forms import SubscriberForm
from subscribers.models import Subscriber

import json


def index(request):
    return render(request, 'subscribers/index.html')


def register(request):
    context = {}
    context['form'] = form = SubscriberForm(request.POST or None)
    return render(request, 'subscribers/register.html', context)


def submit(request):
    form = SubscriberForm(request.POST)
    if form.is_valid():
        form.save()
        return JsonResponse(
            {'success': True},
            json_dumps_params={'indent': 2}
        )
    response = JsonResponse(
        {'success': False, 'errors': form.errors},
        json_dumps_params={'indent': 2}
    )
    response.status_code = 403
    return response


def check_email(request):
    email = request.GET.get('email', '')
    response = {'valid' : None, 'exists': None, 'messages': []}
    validate_max_length = MaxLengthValidator(128)
    try:
        validate_max_length(email)
        validate_email(email)
    except ValidationError as error:
        response['valid'] = False
        response['messages'] = error.messages
    else:
        response['valid'] = True
        if Subscriber.objects.filter(email=email):
            response['exists'] = True
            response['messages'] = Subscriber.unique_error_message(
                Subscriber, Subscriber, ['email']
            ).messages
        else:
            response['exists'] = False
    response['messages'] = [
        message.capitalize() for message in response['messages']
    ]
    return JsonResponse(response)


def list(request):
    return JsonResponse(
        Subscriber.list(),
        json_dumps_params={'indent': 2}
    )


@csrf_protect
def unsubscribe(request):
    email = request.POST.get('email', '')
    password = request.POST.get('password', '')
    query = Subscriber.objects.filter(email=email, password=password)
    try:
        subscriber = query.get()
    except Subscriber.DoesNotExist as error:
        response = JsonResponse({'success': False, 'message': str(error)})
        response.status_code = 403
    else:
        subscriber.delete()
        response = JsonResponse(
            {'success': True, 'message': f'{email} has been unsubscribed.'}
        )
    return response
