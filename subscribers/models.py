from django.core.validators import MinLengthValidator
from django.db import models


class Subscriber(models.Model):
    name = models.CharField("nama", max_length=64)
    email = models.EmailField("alamat surel", max_length=128, unique=True)
    password = models.CharField(
        "kata sandi", max_length=32, validators=[MinLengthValidator(8)]
    )

    class Meta:
        verbose_name = 'pelanggan'
        verbose_name_plural = 'pelanggan-pelanggan'

    @classmethod
    def list(cls):
        return {
            'subscribers': [object.serialize() for object in cls.objects.all()]
        }

    def __str__(self):
        return f"{self.name} ({self.email})"

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'email': self.email
        }
