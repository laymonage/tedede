from django.urls import path
from . import views

app_name = 'subscribers'
urlpatterns = [
    path('', views.index, name="index"),
    path('register/', views.register, name="register"),
    path('register/check_email', views.check_email, name="check_email"),
    path('register/submit', views.submit, name="submit"),
    path('list/', views.list, name="list"),
    path('unsubscribe/', views.unsubscribe, name="unsubscribe")
]
