from django import forms
from subscribers.models import Subscriber


class SubscriberForm(forms.ModelForm):
    class Meta:
        model = Subscriber
        fields = '__all__'
        widgets = {
            'email': forms.EmailInput(
                attrs={'placeholder': 'alamat@surel.com'}
            ),
            'name': forms.TextInput(
                attrs={'placeholder': 'Nama Anda'}
            ),
            'password': forms.PasswordInput(
                attrs={'placeholder': 'Kata sandi, minimal 8 karakter'}
            )
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
            visible.field.label_suffix = ''
