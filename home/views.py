from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.utils import timezone

from status.forms import StatusForm
from status.models import Status


def index(request):
    daftar_status = Status.objects.all().order_by('waktu')
    context = {'daftar_status': daftar_status}

    if request.method == 'POST':
        data = request.POST.copy()
        data['waktu'] = timezone.now()
        form = StatusForm(data)
        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('home:index'))

    else:
        form = StatusForm()

    context['form'] = form

    return render(request, 'home/index.html', context)
