from django.urls import path
from . import views

app_name = 'books'
urlpatterns = [
    path('', views.index, name="index"),
    path('volumes/', views.volumes, name="volumes"),
    path('volumes/<slug:book_id>', views.volumes, name="volumes_details"),
    path('my_books/', views.my_books, name="my_books"),
    path('favorite/', views.favorite, name="favorite"),
    path('unfavorite/', views.unfavorite, name="unfavorite"),
]
