function findIsbn(identifiers) {
    /**
     * Finds ISBN number from identifiers (JSON).
     */
    var isbn;
    if (identifiers) {
        var isbnFound = false;
        $.each(identifiers, function(i, item) {
            if (item.type == 'ISBN_13') {
                isbn = item.identifier;
                isbnFound = true;
                return false;
            }
            if (item.type == 'ISBN_10') {
                isbn = item.identifier;
                isbnFound = true;
            }
        });
        if (!isbnFound) {
            var ids = identifiers;
            isbn = ids[ids.length - 1].identifier;
        }
    } else {
        isbn = '-'
    }
    return isbn;
}


function getBookAndAppendToTable(isAsync, tbody, isFavoriteTable, i, bookId) {
    $.ajax({
        async       : isAsync,
        type        : 'GET',
        url         : 'volumes/' + bookId,
        dataType    : 'json',
    }).then(function(data) {
        appendItemToTable(tbody, isFavoriteTable, i, data);
    });
}


function updateFavoritesCount(items) {
    /**
     * Updates favorites count.
     */
    if (items) {
        $('#favorites-count').text(items.length);
    } else {
        $('#favorites-count').text(0);
    }
}


function toggleBook(bookId) {
    /**
     * Adds/removes book to/from localStorage.
     */
    var favoriteBooks = isLoggedIn === "true" ?
                        sessionStorage.getItem('favoriteBooks') :
                        localStorage.getItem('favoriteBooks');
    favoriteBooks = JSON.parse(favoriteBooks);
    if ($.inArray(bookId, favoriteBooks) != -1) {
        favoriteBooks = $.grep(favoriteBooks, function(value) {
            return value != bookId;
        });
        if (isLoggedIn === "true") {
            $.ajax({
                url: 'unfavorite/',
                type: 'post',
                data: {"book_id": bookId},
                headers: {'X-CSRFToken': Cookies.get('csrftoken')}
            });
        }
        $(`i[book-id="${bookId}"]`).removeClass('fas').addClass('far');
    } else {
        if (!favoriteBooks) {
            favoriteBooks = []
        }
        favoriteBooks.push(bookId);
        var tbody = $('#tbody-favorites');
        if (tbody.find('i[onclick="toggleBook(\'' + bookId + '\')"]').length == 0) {
            getBookAndAppendToTable(true, tbody, true, favoriteBooks.length-1, bookId);
        }
        $('#no-book-favorites').hide();
        $('#book-favorites-available').show();
        $('#book-favorites-table').show();
        if (isLoggedIn === "true") {
            $.ajax({
                url: 'favorite/',
                type: 'post',
                data: {"book_id": bookId},
                headers: {'X-CSRFToken': Cookies.get('csrftoken')}
            });
        }
        $(`i[book-id="${bookId}"]`).removeClass('far').addClass('fas');
    }
    updateFavoritesCount(favoriteBooks);
    if (isLoggedIn === "true") {
        sessionStorage.setItem('favoriteBooks', JSON.stringify(favoriteBooks));
    } else {
        localStorage.setItem('favoriteBooks', JSON.stringify(favoriteBooks));
    }
}


function appendItemToTable(tbody, isFavoriteTable, i, item) {
    /**
     * Appends item to table.
     */
    var info = item.volumeInfo;
    var authors = info.authors ? info.authors : '-';
    var cover = info.imageLinks ? $('<img>').attr(
        {'src':  info.imageLinks.smallThumbnail.replace("http://", "https://")}
    ) : 'Info';
    var link = $('<a>').attr({'href': info.previewLink}).append(cover);
    var isbn = findIsbn(info.industryIdentifiers);
    var favoriteBooks = isLoggedIn === "true" ?
                        sessionStorage.getItem('favoriteBooks') :
                        localStorage.getItem('favoriteBooks');
    favoriteBooks = JSON.parse(favoriteBooks);
    var star = isFavoriteTable ? 'fas' : $.inArray(item.id, favoriteBooks) != -1 ? 'fas' : 'far';
    star = $('<i>').attr({
            'class': star + ' fa-star text-center align-middle',
            'style': 'cursor: pointer;',
            'onclick': `toggleBook('${item.id}')`,
            'book-id': item.id
        });

    var th_no = $('<th>').attr({'scope': 'row'}).text(i + 1);
    var td_title = $('<td>').append($('<p>').text(info.title), $('<p>').text(info.subtitle));
    var td_authors = $('<td>');
    if (authors != '-') {
        $.each(authors, function(i, item) {
            td_authors.append($('<p>').text(item));
        });
    } else {
        td_authors.text(authors);
    }
    var td_pages = $('<td>').text(info.pageCount ? info.pageCount : '-');
    var td_date = $('<td>').append($('<nobr>').text(info.publishedDate));
    var td_isbn = $('<td>').append($('<p>').text(isbn));
    var td_cover = $('<td>').append(link);
    var td_star = $('<td>').append(star);
    var $tr = $('<tr>').append(
        th_no,
        td_title,
        td_authors,
        td_pages,
        td_date,
        td_isbn,
        td_cover,
        td_star
    ).appendTo(tbody);
}

function writeFavoritesTable(items) {
    /**
     * Writes items to favorites table.
     */
    if (!items || items.length == 0) {
        $('#book-favorites-available').hide();
        $('#book-favorites-table').hide();
        return false;
    }
    var tbody = $('#tbody-favorites');
    tbody.empty();
    $('#no-book-favorites').hide();
    $.each(items, function(i, item) {
        getBookAndAppendToTable(false, tbody, true, i, item);
    });
}


function writeResultsTable(items) {
    /**
     * Writes items to results table.
     */
    if (items.errors) {
        $('#results-error').show();
        $('#results-success').hide();
        $('#book-results-table table').hide();
    } else {
        $('#results-error').hide();
        $('#results-success').show();
        $('#book-results-table table').show();
        var tbody = $('#tbody-results');
        tbody.empty();
        $(function() {
            $.each(items, function(i, item) {
                appendItemToTable(tbody, false, i, item);
            });
        });
    }
    $('#book-results').show();
}

$(document).ready(function() {
    isLoggedIn = localStorage.getItem("isLoggedIn");
    $('form').submit(function(event) {

        // get form data
        var formData = {
            'q': $('input[name=q]').val(),
            'maxResults': $('input[name=maxResults]').val()
        };

        // process the form
        $.ajax({
            type        : 'GET',
            url         : 'volumes?' + $.param(formData),
            dataType    : 'json', // expected return type
        })
            // using the done promise callback
            .done(function(data) {

                // log data to the console so we can see
                // console.log(data);
                data = data.error ? data.error : data.items;
                writeResultsTable(data);
            });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });
    var favoriteBooks;
    if (isLoggedIn === "true") {
        $.getJSON('my_books/', function(data) {
            favoriteBooks = data.books;
            sessionStorage.setItem('favoriteBooks', JSON.stringify(favoriteBooks));
            updateFavoritesCount(favoriteBooks);
            writeFavoritesTable(favoriteBooks);
        });
    } else {
        favoriteBooks = JSON.parse(localStorage.getItem('favoriteBooks'));
        if (!favoriteBooks) {
            localStorage.setItem('favoriteBooks', "[]");
            favoriteBooks = [];
        }
        updateFavoritesCount(favoriteBooks);
        writeFavoritesTable(favoriteBooks);
    }
});
