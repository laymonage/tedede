from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class BooksList(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    books = models.TextField("daftar buku", default='{"books": []}')

    class Meta:
        verbose_name = 'daftar buku'
        verbose_name_plural = 'daftar buku'

    def __str__(self):
        return f"Daftar buku {self.user.username}"


@receiver(post_save, sender=User)
def create_user_books_list(sender, instance, created, **kwargs):
    if created:
        BooksList.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_books_list(sender, instance, **kwargs):
    instance.bookslist.save()
