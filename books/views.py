from django.contrib.auth.decorators import login_required
from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.dispatch import receiver
from django.http import HttpResponseForbidden, JsonResponse
from django.shortcuts import render
import json
import requests


def index(request):
    return render(request, 'books/index.html')


def volumes(request, book_id=''):
    url = 'https://www.googleapis.com/books/v1/volumes'
    if book_id:
        url += f'/{book_id}'
    else:
        maxResults = request.GET.get('maxResults', '')
        if not maxResults:
            maxResults = 10
        url += f"?maxResults={maxResults}&q={request.GET.get('q', '')}"
    data = requests.get(url).json()
    return JsonResponse(data, json_dumps_params={'indent': 2})


@login_required
def my_books(request):
    return JsonResponse({"books": request.session['books']})


@login_required
def favorite(request):
    book_id = request.POST.get('book_id', '')
    if not book_id:
        return JsonResponse({'success': False})
    if book_id not in request.session['books']:
        request.session['books'].append(book_id)
        request.session.modified = True
    return JsonResponse({'success': True})


@login_required
def unfavorite(request):
    book_id = request.POST.get('book_id', '')
    if not book_id:
        return JsonResponse({'success': False})
    if book_id in request.session['books']:
        request.session['books'].remove(book_id)
        request.session.modified = True
    return JsonResponse({'success': True})


@receiver(user_logged_in)
def create_books_set(sender, user, request, **kwargs):
    request.session['books'] = json.loads(user.bookslist.books)['books']


@receiver(user_logged_out)
def save_books_set(sender, user, request, **kwargs):
    user.bookslist.books = json.dumps(
        {"books": [book for book in request.session['books']]}
    )
    user.save()
