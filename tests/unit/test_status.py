from django.core.exceptions import ValidationError
from django.test import TestCase
from django.utils import timezone
from status import models
from status import forms


class StatusTestCase(TestCase):
    def test_model_can_create_new_status(self):
        new_status = models.Status.objects.create(
            isi_status="Capek banget :(",
            waktu=timezone.now()
        )
        all_status_count = models.Status.objects.all().count()
        self.assertEqual(all_status_count, 1)

    def test_model_status_no_longer_than_128(self):
        new_status = models.Status(
            isi_status="Ha"*128,
            waktu=timezone.now()
        )
        self.assertRaises(ValidationError, new_status.full_clean)

    def test_model_status_string(self):
        message = "Sebuah status"
        new_status = models.Status(
            isi_status=message,
            waktu=timezone.now()
        )
        self.assertEqual(f"{new_status.waktu}: {message}", str(new_status))

    def test_form_validation_for_blank_items(self):
        form = forms.StatusForm(data={'isi_status': '', 'waktu': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['isi_status'],
            ["Bidang ini tidak boleh kosong."]
        )
        self.assertEqual(
            form.errors['waktu'],
            ["Bidang ini tidak boleh kosong."]
        )
