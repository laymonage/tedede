from .meta import UserTestCase
from django.urls import resolve, reverse

from oauth import views


class OAuthTestCase(UserTestCase):
    def test_login_url_exists(self):
        response = self.client.get(reverse('oauth:login'))
        self.assertEqual(response.status_code, 200)

    def test_login_uses_login_function(self):
        found = resolve(reverse('oauth:login'))
        self.assertEqual(found.func, views.login)

    def test_login_uses_login_template(self):
        response = self.client.get(reverse('oauth:login'))
        self.assertTemplateUsed(response, 'oauth/login.html')

    def test_login_redirect_authenticated_user(self):
        self.client.login(username="pewe", password="ppw_csui")
        response = self.client.get(reverse('oauth:login'))
        self.assertEqual(response.status_code, 302)

    def test_is_logged_in(self):
        response = self.client.get(reverse('oauth:is_logged_in'))
        self.assertJSONEqual(response.content, {"loggedIn": False})
        self.client.login(username="pewe", password="ppw_csui")
        response = self.client.get(reverse('oauth:is_logged_in'))
        self.assertJSONEqual(response.content, {"loggedIn": True})

    def test_welcome_url_exists(self):
        response = self.client.get(reverse('oauth:welcome'))
        self.assertEqual(response.status_code, 302)
        self.client.login(username="pewe", password="ppw_csui")
        response = self.client.get(reverse('oauth:welcome'))
        self.assertEqual(response.status_code, 200)

    def test_welcome_uses_welcome_function(self):
        found = resolve(reverse('oauth:welcome'))
        self.assertEqual(found.func, views.welcome)

    def test_welcome_uses_welcome_template(self):
        self.client.login(username="pewe", password="ppw_csui")
        response = self.client.get(reverse('oauth:welcome'))
        self.assertTemplateUsed(response, 'oauth/welcome.html')

    def test_logout_redirect_unauthenticated_user(self):
        response = self.client.get(reverse('oauth:logout'))
        self.assertEqual(response.status_code, 302)

    def test_logout_can_logout(self):
        self.client.login(username="pewe", password="ppw_csui")
        self.client.get(reverse('oauth:logout'))
        response = self.client.get(reverse('oauth:is_logged_in'))
        self.assertJSONEqual(response.content, {"loggedIn": False})
