from django.test import TestCase
from django.urls import resolve, reverse

from subscribers import models
from subscribers import views

import json


class SubscribersTestCase(TestCase):
    def test_model_subscriber_string(self):
        name, email, password = 'test bed', 'test@email.com', 'itsvalid123'
        new_sub = models.Subscriber(
            name=name, email=email, password=password
        )
        self.assertEqual(str(new_sub), f"{new_sub.name} ({new_sub.email})")

    def test_model_subscriber_serialize(self):
        name, email, password = 'test bed', 'test@email.com', 'itsvalid123'
        new_sub = models.Subscriber(
            name=name, email=email, password=password
        )
        new_sub.save()
        self.assertEqual(
            new_sub.serialize(), {'id': 1, 'name': name, 'email': email}
        )
        self.assertEqual(
            models.Subscriber.list(),
            {'subscribers': [{'id': 1,'name': name, 'email': email}]}
        )

    def test_index_url_exists(self):
        response = self.client.get(reverse('subscribers:index'))
        self.assertEqual(response.status_code, 200)

    def test_index_uses_index_function(self):
        found = resolve(reverse('subscribers:index'))
        self.assertEqual(found.func, views.index)

    def test_index_uses_index_template(self):
        response = self.client.get(reverse('subscribers:index'))
        self.assertTemplateUsed(response, 'subscribers/index.html')

    def test_list_url_exists(self):
        response = self.client.get(reverse('subscribers:list'))
        self.assertEqual(response.status_code, 200)

    def test_list_uses_list_function(self):
        found = resolve(reverse('subscribers:list'))
        self.assertEqual(found.func, views.list)

    def test_list_shows_subscribers_list(self):
        name, email, password = 'test bed', 'test@email.com', 'itsvalid123'
        new_sub = models.Subscriber(
            name=name, email=email, password=password
        )
        new_sub.save()
        response = self.client.get(reverse('subscribers:list'))
        content = response.content.decode('utf-8')
        self.assertEqual(
            content, json.dumps(models.Subscriber.list(), indent=2)
        )

    def test_register_url_exists(self):
        response = self.client.get(reverse('subscribers:register'))
        self.assertEqual(response.status_code, 200)

    def test_register_uses_register_function(self):
        found = resolve(reverse('subscribers:register'))
        self.assertEqual(found.func, views.register)

    def test_register_uses_register_template(self):
        response = self.client.get(reverse('subscribers:register'))
        self.assertTemplateUsed(response, 'subscribers/register.html')

    def test_daftar_in_register(self):
        text = "Daftar"
        response = self.client.get(reverse('subscribers:register'))
        content = response.content.decode('utf8')
        self.assertIn(text, content)

    def test_register_form_in_register(self):
        response = self.client.get(reverse('subscribers:register'))
        content = response.content.decode('utf8')
        self.assertIn('<form', content)
        self.assertIn('method="post">', content)
        self.assertIn(' for="name">Nama</label>', content)
        self.assertIn('<input type="text"', content)
        self.assertIn('id="id_name"', content)
        self.assertIn(' for="email">Alamat surel</label>', content)
        self.assertIn('<input type="email"', content)
        self.assertIn('id="id_email"', content)
        self.assertIn(' for="password">Kata sandi</label>', content)
        self.assertIn('<input type="password"', content)
        self.assertIn('id="id_password"', content)

    def test_register_check_email_url_exists(self):
        response = self.client.get(reverse('subscribers:check_email'))
        self.assertEqual(response.status_code, 200)

    def test_register_check_email_uses_check_email_function(self):
        found = resolve(reverse('subscribers:check_email'))
        self.assertEqual(found.func, views.check_email)

    def test_register_check_email_can_check_email(self):
        email = 'alamat'
        base_url = reverse('subscribers:check_email')
        response = self.client.get(base_url + f'?email={email}')
        self.assertEqual(
            json.loads(response.content),
            {'valid': False, 'exists': None, 'messages': [
                'Masukkan alamat email yang valid.'
            ]}
        )
        email = 'alamat@surel.com'
        response = self.client.get(base_url + f'?email={email}')
        self.assertEqual(
            json.loads(response.content),
            {'valid': True, 'exists': False, 'messages': []}
        )
        models.Subscriber.objects.create(
            name="test bed",
            email=email,
            password="0xdeadbeef"
        )
        response = self.client.get(base_url + f'?email={email}')
        self.assertEqual(
            json.loads(response.content),
            {'valid': True, 'exists': True, 'messages': [
                "Pelanggan dengan alamat surel telah ada."
            ]}
        )

    def test_register_submit_url_exists(self):
        response = self.client.get(reverse('subscribers:submit'))
        self.assertEqual(response.status_code, 403)

    def test_register_submit_uses_submit_function(self):
        found = resolve(reverse('subscribers:submit'))
        self.assertEqual(found.func, views.submit)

    def test_register_submit_can_create_model(self):
        name, email, password = 'test bed', 'test@email.com', 'itsvalid123'
        response = self.client.post(
            reverse('subscribers:submit'),
            data={
                'name': name,
                'email': email,
                'password': password
            }
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(models.Subscriber.objects.all().count(), 1)

    def test_unsubscribe_url_exists(self):
        response = self.client.get(reverse('subscribers:unsubscribe'))
        self.assertNotEqual(response.status_code, 404)

    def test_unsubscribe_uses_unsubscribe_function(self):
        found = resolve(reverse('subscribers:unsubscribe'))
        self.assertEqual(found.func, views.unsubscribe)

    def test_unsubscribe_can_unsubscribe(self):
        name, email, password = 'test bed', 'test@email.com', 'itsvalid123'
        new_sub = models.Subscriber(
            name=name, email=email, password=password
        )
        self.assertEqual(models.Subscriber.objects.all().count(), 0)
        new_sub.save()
        self.assertEqual(models.Subscriber.objects.all().count(), 1)
        response = self.client.post(
            reverse('subscribers:unsubscribe'),
            data={'email': email, 'password': password}
        )
        self.assertEqual(models.Subscriber.objects.all().count(), 0)
