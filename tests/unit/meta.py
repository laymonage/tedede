from django.contrib.auth.models import User
from django.test import TestCase


class UserTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.user = User(username="pewe")
        self.user.set_password("ppw_csui")
        self.user.save()
