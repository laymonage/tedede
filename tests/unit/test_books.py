from .meta import UserTestCase
from django.test import TestCase
from django.urls import resolve, reverse

from books import models, views

import json


class BooksTestCase(TestCase):
    def test_books_url_exists(self):
        response = self.client.get(reverse('books:index'))
        self.assertEqual(response.status_code, 200)

    def test_books_uses_index_function(self):
        found = resolve(reverse('books:index'))
        self.assertEqual(found.func, views.index)

    def test_books_uses_index_template(self):
        response = self.client.get(reverse('books:index'))
        self.assertTemplateUsed(response, 'books/index.html')

    def test_cari_buku_in_books(self):
        text = "Cari buku"
        response = self.client.get(reverse('books:index'))
        content = response.content.decode('utf8')
        self.assertIn(text, content)

    def test_find_books_form_in_books(self):
        response = self.client.get(reverse('books:index'))
        content = response.content.decode('utf8')
        self.assertIn('<form', content)
        self.assertIn('method="get">', content)
        self.assertIn(' for="q">Kata kunci:</label>', content)
        self.assertIn('<input type="text"', content)
        self.assertIn('id="id_q"', content)
        self.assertIn(' for="maxResults">Batas:</label>', content)
        self.assertIn('<input type="number"', content)
        self.assertIn('id="id_maxResults"', content)

    def test_no_favorites_available(self):
        response = self.client.get(reverse('books:index'))
        content = response.content.decode('utf8')
        self.assertIn('Belum ada buku favorit.', content)

    def test_books_volumes_url_exists(self):
        response = self.client.get(reverse('books:volumes'))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse(
            'books:volumes_details',
            kwargs={'book_id': 'j24GMN0OtS8C'}))
        self.assertEqual(response.status_code, 200)

    def test_books_volumes_uses_volumes_function(self):
        found = resolve(reverse('books:volumes'))
        self.assertEqual(found.func, views.volumes)
        found = resolve(reverse(
            'books:volumes_details',
            kwargs={'book_id': 'j24GMN0OtS8C'}))
        self.assertEqual(found.func, views.volumes)

    def test_volumes_views_can_access_google_books_api(self):
        q = 'lemony snicket'
        url = reverse('books:volumes') + f'?q={q}'
        response = self.client.get(url)
        self.assertLessEqual(len(json.loads(response.content)), 10)
        maxResults = 40
        url += f'&maxResults={maxResults}'
        response = self.client.get(url)
        self.assertLessEqual(len(json.loads(response.content)), 40)


class BooksListTestCase(UserTestCase):
    def test_user_has_books(self):
        self.assertJSONEqual(
            self.user.bookslist.books, {"books": []}
        )
        self.assertEqual(
            str(self.user.bookslist), f"Daftar buku {self.user.username}"
        )

    def test_unauthenticated_user_cant_access_my_books(self):
        response = self.client.get(reverse('books:my_books'))
        self.assertEqual(response.status_code, 302)

    def test_authenticated_user_can_access_my_books(self):
        self.client.login(username="pewe", password="ppw_csui")
        response = self.client.get(reverse('books:my_books'))
        self.assertJSONEqual(response.content, {"books": []})

    def test_unauthenticated_user_cant_access_favorite_and_unfavorite(self):
        response = self.client.post(reverse('books:favorite'))
        self.assertEqual(response.status_code, 302)
        response = self.client.post(reverse('books:unfavorite'))
        self.assertEqual(response.status_code, 302)

    def test_authenticated_user_can_favorite_and_unfavorite(self):
        self.client.login(username="pewe", password="ppw_csui")
        response = self.client.post(reverse('books:favorite'))
        self.assertJSONEqual(response.content, {"success": False})
        response = self.client.post(reverse('books:unfavorite'))
        self.assertJSONEqual(response.content, {"success": False})

        response = self.client.post(
            reverse('books:favorite'),
            data={"book_id": "sebuahIDBuku123"}
        )
        self.assertJSONEqual(response.content, {"success": True})
        books = json.loads(
            self.client.get(reverse('books:my_books')).content
        )['books']
        self.assertIn("sebuahIDBuku123", books)

        response = self.client.post(
            reverse('books:unfavorite'),
            data={"book_id": "sebuahIDBuku123"}
        )
        self.assertJSONEqual(response.content, {"success": True})
        books = json.loads(
            self.client.get(reverse('books:my_books')).content
        )['books']
        self.assertNotIn("sebuahIDBuku123", books)

    def test_save_session_to_model_on_logout(self):
        self.client.login(username="pewe", password="ppw_csui")
        self.client.post(
            reverse('books:favorite'),
            data={"book_id": "sebuahIDBuku123"}
        )
        self.user.refresh_from_db()
        books = json.loads(self.user.bookslist.books)['books']
        self.assertNotIn("sebuahIDBuku123", books)

        self.client.logout()
        self.user.refresh_from_db()
        books = json.loads(self.user.bookslist.books)['books']
        self.assertIn("sebuahIDBuku123", books)
