from django.test import TestCase
from django.urls import resolve, reverse
from django.utils import timezone

from status.forms import StatusForm
from home import views


class HomeTestCase(TestCase):
    def test_home_url_exists(self):
        response = self.client.get(reverse('home:index'))
        self.assertEqual(response.status_code, 200)

    def test_home_uses_index_function(self):
        found = resolve(reverse('home:index'))
        self.assertEqual(found.func, views.index)

    def test_home_uses_index_template(self):
        response = self.client.get(reverse('home:index'))
        self.assertTemplateUsed(response, 'home/index.html')

    def test_halo_apa_kabar_in_home(self):
        text = "Halo, apa kabar?"
        response = self.client.get(reverse('home:index'))
        content = response.content.decode('utf8')
        self.assertIn(text, content)

    def test_input_status_form_in_home(self):
        response = self.client.get(reverse('home:index'))
        content = response.content.decode('utf8')
        self.assertIn('<form', content)
        self.assertIn('method="post">', content)
        self.assertIn(' for="isi_status">Status</label>', content)
        self.assertIn('<input type="text"', content)
        self.assertIn('id="id_isi_status"', content)

    def test_no_status_available(self):
        response = self.client.get(reverse('home:index'))
        content = response.content.decode('utf8')
        self.assertIn('Belum ada status.', content)
        self.assertNotIn('<table', content)

    def test_status_post_success_and_render_the_result(self):
        message = 'Lelah'
        response_post = self.client.post(
            reverse('home:index'),
            {'isi_status': message, 'waktu': timezone.now()}
        )
        self.assertEqual(response_post.status_code, 302)

        response = self.client.get(reverse('home:index'))
        content = response.content.decode('utf8')
        self.assertNotIn('Belum ada status.', content)
        self.assertIn(message, content)
