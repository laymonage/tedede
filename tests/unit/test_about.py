from django.test import TestCase
from django.urls import resolve, reverse

from about import views


class AboutTestCase(TestCase):
    def test_about_url_exists(self):
        response = self.client.get(reverse('about:index'))
        self.assertEqual(response.status_code, 200)

    def test_about_uses_index_function(self):
        found = resolve(reverse('about:index'))
        self.assertEqual(found.func, views.index)

    def test_about_uses_index_template(self):
        response = self.client.get(reverse('about:index'))
        self.assertTemplateUsed(response, 'about/index.html')

    def test_my_name_in_about(self):
        text = "Sage Muhammad Abdullah"
        response = self.client.get(reverse('about:index'))
        content = response.content.decode('utf8')
        self.assertIn(text, content)

    def test_progress_bar_in_about(self):
        text = '<div class="progress-bar" role="progressbar"'
        response = self.client.get(reverse('about:index'))
        content = response.content.decode('utf8')
        self.assertIn(text, content)
