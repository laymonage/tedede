from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver


class FunctionalTestCase(StaticLiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        chrome_options = webdriver.chrome.options.Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=chrome_options)
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class FunctionalDataTestCase(FunctionalTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.selenium.get(cls.live_server_url)
        status_input = cls.selenium.find_element_by_name("isi_status")
        status_input.send_keys("Sebuah status")
        cls.selenium.find_element_by_xpath('//button[@type="submit"]').click()


class element_has_css_property_value(object):
  '''
  An expectation for checking that an element
  has a particular css property value.

  locator - used to find the element
  returns the WebElement once it has the particular css property value
  '''
  def __init__(self, locator, css_property, css_value):
    self.locator = locator
    self.css_property = css_property
    self.css_value = css_value

  def __call__(self, driver):
    element = driver.find_element(*self.locator)
    if self.css_value == element.value_of_css_property(self.css_property):
        return element
    else:
        return False
