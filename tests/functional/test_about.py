from .meta import FunctionalTestCase
from time import sleep


class AboutLayoutTestCase(FunctionalTestCase):

    def test_about_page_layout_header(self):
        self.selenium.get(f"{self.live_server_url}/about/")
        header = self.selenium.find_element_by_id("header-text")
        p_list = header.find_elements_by_tag_name('p')
        self.assertIn("Sage Muhammad Abdullah", p_list[0].text)
        self.assertIn("laymonage", p_list[1].text)
        self.assertIn("Jakarta, Indonesia", p_list[2].text)

    def test_about_page_layout_description(self):
        self.selenium.get(f"{self.live_server_url}/about/")
        description = self.selenium.find_element_by_id('description')
        self.assertIn('mahasiswa', description.text)

    def test_about_page_layout_skills(self):
        self.selenium.get(f"{self.live_server_url}/about/")
        skills = self.selenium.find_element_by_id('skills')
        skills_list = skills.find_elements_by_id('skill')
        self.assertEqual("Python", skills_list[0].text)
        self.assertEqual("Java", skills_list[1].text)
        self.assertEqual("Desain Grafis", skills_list[2].text)
        self.assertEqual("Fotografi", skills_list[3].text)
        self.assertEqual("Bahasa Inggris", skills_list[4].text)
        self.assertEqual("Bahasa Indonesia", skills_list[5].text)

    def test_can_expand_collapse_accordion(self):
        self.selenium.get(f"{self.live_server_url}/about/")
        accordion = self.selenium.find_element_by_class_name('accordion')
        cards = accordion.find_elements_by_class_name('card')
        for card in cards:
            collapse = card.find_element_by_class_name('collapse')
            display = collapse.value_of_css_property('display')
            self.assertEqual(display, 'none')
            card.find_element_by_tag_name('button').click()
            sleep(1)
            collapse = card.find_element_by_class_name('collapse')
            display = collapse.value_of_css_property('display')
            self.assertEqual(display, 'block')


class AboutStyleTestCase(FunctionalTestCase):

    def test_about_page_style_header(self):
        self.selenium.get(f"{self.live_server_url}/about/")
        header = self.selenium.find_element_by_id('header-text')
        p_list = header.find_elements_by_tag_name('p')
        for p in p_list:
            self.assertIn('font-weight-medium', p.get_attribute('class'))
        self.assertIn('display-5', p_list[0].get_attribute('class'))
        self.assertIn('text-primary', p_list[0].get_attribute('class'))
        self.assertIn('lead', p_list[1].get_attribute('class'))
        self.assertIn('text-secondary', p_list[1].get_attribute('class'))
        self.assertIn('lead', p_list[2].get_attribute('class'))

    def test_about_page_style_description(self):
        self.selenium.get(f"{self.live_server_url}/about/")
        description = self.selenium.find_element_by_id('description')
        p = description.find_element_by_tag_name('p')
        self.assertEqual(
            "serif text-secondary text-justify", p.get_attribute('class')
        )

    def test_about_page_style_skills(self):
        self.selenium.get(f"{self.live_server_url}/about/")
        skills = self.selenium.find_element_by_id('skills')
        skills_list = skills.find_elements_by_id('skill')
        for skill in skills_list:
            self.assertIsNotNone(skill.find_element_by_tag_name('h4'))
            progress = skill.find_element_by_class_name('progress')
            self.assertIsNotNone(
                progress.find_element_by_class_name('progress-bar')
            )
