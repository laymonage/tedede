from .meta import FunctionalDataTestCase, element_has_css_property_value
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait

class ModeTestCase(FunctionalDataTestCase):

    def test_can_change_color_mode(self):
        self.selenium.get(self.live_server_url)
        body = self.selenium.find_element_by_tag_name('body')
        background_color = body.value_of_css_property('background-color')
        color = body.value_of_css_property('color')
        self.assertEqual(background_color, 'rgba(255, 255, 255, 1)')
        self.assertEqual(color, 'rgba(33, 33, 33, 1)')
        table = self.selenium.find_element_by_tag_name('table')
        thead = self.selenium.find_element_by_tag_name('thead')
        self.assertEqual(table.get_attribute('class'), 'table')
        self.assertEqual(thead.get_attribute('class'), 'thead-light')

        self.selenium.find_element_by_id('mode-button').click()

        wait = WebDriverWait(self.selenium, 1)
        body = wait.until(element_has_css_property_value(
            (By.TAG_NAME, 'body'), 'background-color', 'rgba(23, 29, 35, 1)')
        )
        background_color = body.value_of_css_property('background-color')
        color = body.value_of_css_property('color')
        self.assertEqual(background_color, 'rgba(23, 29, 35, 1)')
        self.assertEqual(color, 'rgba(222, 222, 222, 1)')
        table = self.selenium.find_element_by_tag_name('table')
        thead = self.selenium.find_element_by_tag_name('thead')
        self.assertEqual(table.get_attribute('class'), 'table table-dark')
        self.assertEqual(thead.get_attribute('class'), 'thead-dark')
