from .meta import FunctionalTestCase


class StatusTestCase(FunctionalTestCase):

    def test_input_status(self):
        self.selenium.get(self.live_server_url)
        self.assertIn("Belum ada status.", self.selenium.page_source)
        self.assertNotIn("Coba Coba", self.selenium.page_source)
        status_input = self.selenium.find_element_by_name("isi_status")
        status_input.send_keys("Coba Coba")
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        self.assertIn("Coba Coba", self.selenium.page_source)
        self.assertNotIn("Belum ada status.", self.selenium.page_source)
