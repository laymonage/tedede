from django.db import models


class Status(models.Model):
    isi_status = models.CharField("Status", max_length=128)
    waktu = models.DateTimeField("Waktu")

    class Meta:
        verbose_name = 'Status'
        verbose_name_plural = 'Status-status'

    def __str__(self):
        return f"{self.waktu}: {self.isi_status}"
